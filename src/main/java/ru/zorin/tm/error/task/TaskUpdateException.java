package ru.zorin.tm.error.task;

import ru.zorin.tm.error.AbstractException;

public class TaskUpdateException extends AbstractException {

    public TaskUpdateException() {
        super("Fail to update task");
    }
}