package ru.zorin.tm.error;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }
}