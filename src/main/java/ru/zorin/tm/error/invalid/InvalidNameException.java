package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidNameException extends AbstractException {

    public InvalidNameException() {
        super("Invalid name");
    }
}