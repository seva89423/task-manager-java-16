package ru.zorin.tm.api.service;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IProjectService getProjectService();
}