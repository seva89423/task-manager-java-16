package ru.zorin.tm.api.repository;

import ru.zorin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add (String userId, Task task);

    void remove(String userId, Task task);

    List<Task> findAll(String userId);

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task getTaskByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);
}
