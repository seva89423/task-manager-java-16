package ru.zorin.tm.api.controller;

public interface ICommandController {

     void showHelp();

     void showAbout();

     void showVersion();

     void showCommands();

     void showArguments();

     void showInfo();
}
