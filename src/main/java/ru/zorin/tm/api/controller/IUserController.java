package ru.zorin.tm.api.controller;

import ru.zorin.tm.entity.User;

public interface IUserController {

    void updateUserById();

    void updateUserByLogin();

    void showUserById();

    void showUserByLogin();

    void showUser(User user);

    void removeUserById();

    void removeUserByLogin();
}