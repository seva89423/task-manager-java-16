package ru.zorin.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_ABOUT = "about";

    String CMD_VERSION = "version";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_ARGUMENTS = "arguments";

    String CMD_COMMANDS = "commands";

    String TASK_LIST = "task-list";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String PROJECT_LIST = "project-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_VIEW_BY_NAME = "task-view-by-name";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String LOGIN = "login";

    String LOGOUT = "logout";

    String REGISTRY = "registry";

    String USER_UPDATE_BY_ID = "update-user-by-id";

    String USER_SHOW_BY_ID = "show-user-by-id";

    String USER_SHOW_BY_LOGIN = "show-user-by-login";

    String USER_UPDATE_BY_LOGIN = "update-user-by-login";

    String USER_REMOVE_BY_ID = "remove-user-by-id";

    String USER_REMOVE_BY_LOGIN = "remove-user-by-login";
}