package ru.zorin.tm.bootstrap;
import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.api.repository.IUserRepository;
import ru.zorin.tm.api.service.*;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.error.invalid.InvalidCommandException;
import ru.zorin.tm.repository.CommandRepository;
import ru.zorin.tm.repository.ProjectRepository;
import ru.zorin.tm.repository.TaskRepository;
import ru.zorin.tm.repository.UserRepository;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.service.*;
import ru.zorin.tm.util.TerminalUtil;

import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    {
        initialize(commandService.getTermCommands());
    }

    private void initialize(List<AbstractCommand> commands) {
        for(AbstractCommand command: commands) registryCommand(command);
    }

    private void registryCommand(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUser() {
        userService.create("test", "test", "test@email.com");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        initUser();
        parseArg(args);
        inputCommand();
    }

    private void inputCommand() {
    while (true) {
        try {
            parseCommand(TerminalUtil.nextLine());
        } catch (Exception e) {
            System.err.println("[ERROR]");
            System.err.println(e.getMessage());
            }
        }
    }

    private void parseCommand(final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new InvalidCommandException();
        authService.checkRole(command.roles());
        command.execute();
    }

    private boolean parseArg(final String... args) throws Exception {
        if (args == null || args.length == 0) return false;
        for (String arg : args) validateArg(arg);
        return true;
    }

    private void validateArg(final String args) throws Exception {
        final AbstractCommand argument = arguments.get(args);
        if (arguments.get(args) == null) throw new InvalidCommandException();
        argument.execute();
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArgs() {
        return arguments.values();
    }
}