package ru.zorin.tm.service;

import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.api.service.ICommandService;
import ru.zorin.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getTermCommands() {
        return commandRepository.getTermCommands();
    }
}