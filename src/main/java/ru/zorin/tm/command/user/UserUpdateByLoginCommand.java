package ru.zorin.tm.command.user;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.util.HashUtil;
import ru.zorin.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-user-by-login";
    }

    @Override
    public String description() {
        return "Update user by login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER OLD LOGIN:");
        final String login = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new InvalidLoginException();
        System.out.println("ENTER NEW LOGIN:");
        final String newLogin = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateByLogin(userId, newLogin, HashUtil.salt(password));
        if (userUpdated == null) throw new InvalidUserIdException();
        System.out.println("[COMPLETE]");
    }
}