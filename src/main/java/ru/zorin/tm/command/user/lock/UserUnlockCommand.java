package ru.zorin.tm.command.user.lock;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.error.invalid.InvalidNameException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "unlock-user";
    }

    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER USER LOGIN TO UNLOCK");
        final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        serviceLocator.getUserService().unlockServiceByLogin(login);
        System.out.println("[USER IS UNLOCKED]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
