package ru.zorin.tm.command.user.lock;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.error.invalid.InvalidNameException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public String description() {
        return "Lock user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER USER LOGIN TO LOCK");
        final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        serviceLocator.getUserService().lockServiceByLogin(login);
        System.out.println("[USER IS LOCKED]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
