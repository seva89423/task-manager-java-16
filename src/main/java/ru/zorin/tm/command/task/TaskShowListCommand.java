package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;

import java.util.List;

public class TaskShowListCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show list of tasks";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[COMPLETE]");
    }
}