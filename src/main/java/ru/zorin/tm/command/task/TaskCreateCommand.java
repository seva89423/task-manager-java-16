package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-create-new";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[COMPLETE]");
    }
}