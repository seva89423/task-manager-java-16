package ru.zorin.tm.command.task;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "remove-task-by-id";
    }

    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeOneById(userId, id);
        if (task == null) throw new TaskEmptyException();
        else System.out.println("[COMPLETE]");
    }
}